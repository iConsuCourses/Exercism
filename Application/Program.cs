﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static System.Console;

namespace Application
{
    class Solution
    {
        public static string Hello() => "Hello, World!";

        public static bool IsLeapYear(int year) => year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);

        public static string Reverse(string input)
        {
            char[] array = input.ToCharArray();
            Array.Reverse(array);
            return new string(array);
        }

        public static bool IsPangram(string input)
        {
            bool flag = false;
            byte a = 97;
            byte[] alphabetBytes = Encoding.UTF8.GetBytes(input.ToLower().ToCharArray());
            for(int i = 0; i < alphabetBytes.Length; i++)
            {
                if (alphabetBytes.Contains(a))
                {
                    a++;
                    if (a.Equals(122))
                        break;
                }
                else
                {
                    flag = false;
                    break;
                }
                flag = true;
            }
            return flag;
        }

        public static DateTime Add(DateTime birthDate) => birthDate.AddSeconds(Math.Pow(10, 9));

        public static int[] Factors(long number)
        {
            List<int> array = new List<int>();
            for(int x = 2; number > 1;)
            {
                if (number % x == 0)
                {
                    array.Add(x);
                    number /= x;
                    x = 2;
                }
                else
                    x++;
            }
            return array.ToArray();
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Write("Menú: ");
            byte x = byte.Parse(ReadLine());
            int a;
            string str;
            switch(x)
            {
                case 1:
                    WriteLine(Solution.Hello());
                    break;
                case 2:
                    Write("Año: ");
                    a = int.Parse(ReadLine());
                    WriteLine(Solution.IsLeapYear(a));
                    break;
                case 3:
                    Write("Texto: ");
                    str = ReadLine();
                    WriteLine(Solution.Reverse(str));
                    break;
                case 4:
                    WriteLine(Solution.IsPangram(""));
                    break;
                case 5:
                    WriteLine(Solution.Add(new DateTime(2011, 4, 25)));
                    WriteLine(Solution.Add(new DateTime(1977, 6, 13)));
                    WriteLine(Solution.Add(new DateTime(1959, 7, 19)));
                    WriteLine(Solution.Add(new DateTime(2015, 1, 24, 22, 0, 0)));
                    WriteLine(Solution.Add(new DateTime(2015, 1, 24, 23, 59, 59)));
                    break;
                case 6:
                    WriteLine(Solution.Factors(2));
                    WriteLine(Solution.Factors(9));
                    WriteLine(Solution.Factors(8));
                    WriteLine(Solution.Factors(12));
                    WriteLine(Solution.Factors(901255));
                    WriteLine(Solution.Factors(93819012551));
                    break;
            }
            ReadKey();
        }
    }
}
